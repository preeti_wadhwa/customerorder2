﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CustomerDetail.Models;

namespace CustomerDetail.Controllers
{
    public class OrderController : Controller
    {
        private CustomerOrderContext db = new CustomerOrderContext();

        //
        // GET: /Order/

        //public ActionResult Index()
        //{
        //    var orders = db.Orders.Include(o => o.Customer);
        //    return View(orders.ToList());
        //}

        public ActionResult Index(int Cust_Id)
        {
            var orders = db.Orders.Where(o => o.Customer_Id == Cust_Id);
            return View(orders);
        }

        //
        // GET: /Order/Details/5

        public ActionResult Details(int id = 0)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        //
        // GET: /Order/Create

        public ActionResult Create()
        {
            ViewBag.Customer_Id = new SelectList(db.Customers, "Cust_Id", "Name");
            return View();
        }

        //
        // POST: /Order/Create

        [HttpPost]
        public ActionResult Create(Order order)
        {
            if (ModelState.IsValid)
            {
                db.Orders.Add(order);
                db.SaveChanges();
                return RedirectToAction("Index","Customer");
            }

            ViewBag.Customer_Id = new SelectList(db.Customers, "Cust_Id", "Name", order.Customer_Id);
            return View(order);
        }

        //
        // GET: /Order/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            ViewBag.Customer_Id = new SelectList(db.Customers, "Cust_Id", "Name", order.Customer_Id);
            return View(order);
        }

        //
        // POST: /Order/Edit/5

        [HttpPost]
        public ActionResult Edit(Order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Customer");
            }
            ViewBag.Customer_Id = new SelectList(db.Customers, "Cust_Id", "Name", order.Customer_Id);
            return View(order);
        }

        //
        // GET: /Order/Delete/5

        public ActionResult Delete(int id)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        //
        // POST: /Order/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = db.Orders.Find(id);
            db.Orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index","Customer");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}